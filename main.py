import base64
import json
import logging

import flask

app = flask.Flask(__name__)


@app.route('/')
def index():
    return 'Hello!'

@app.route('/webhooks/<sub>', methods=["POST"])
def webhooks(sub):
    logger = logging.getLogger(f"Webhook {sub}")
    b64_data = flask.request.json["message"]["data"]
    json_data = base64.b64decode(b64_data)
    logger.warning(json_data)
    return "OK"

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=False)
